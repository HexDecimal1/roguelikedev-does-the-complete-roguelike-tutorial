#!/usr/bin/env python3

import sys

sys.path.insert(0, 'src/')

from engine import main

if __name__ == '__main__':
    main()
