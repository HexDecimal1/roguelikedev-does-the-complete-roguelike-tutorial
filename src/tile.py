
import numpy as np

TILE_DTYPE = [
    ('ch', np.intc),
    ('fg', '3uint8'),
    ('bg', '3uint8'),
    ('dark_bg', '3uint8'),
    ('transparent', np.bool_),
    ('move_cost', np.uint8),
]

wall = ((ord(' '), [0, 0, 0], [130, 110, 50], [0, 0, 100], False, 0))
floor = ((ord(' '), [0, 0, 0], [200, 180, 50], [50, 50, 150], True, 1))
