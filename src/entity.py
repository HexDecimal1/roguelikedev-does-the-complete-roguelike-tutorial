
import math

import tcod

import g
import state

class Entity(object):

    def __init__(self, location, ch, fg, name, blocking=False, render_order=3,
                 fighter=None, ai=None):
        self._location = None
        self.location = location
        self.ch = ch
        self.fg = fg
        self.name = name
        self.blocking = blocking
        self.render_order = render_order
        self.fighter = fighter
        self.ai = ai

        if self.fighter:
            self.fighter.owner = self

        if self.ai:
            self.ai.owner = self

    @property
    def location(self):
        return self._location
    @location.setter
    def location(self, new_loc):
        if self._location:
            self._location.contents.remove(self)
        if new_loc:
            new_loc.contents.append(self)
        self._location = new_loc
        # Recompute fov immediately if the player has moved.
        if self is g.player and self._location:
            g.fov_needs_regeneration = True

    def attack_move_to(self, x:int, y:int) -> None:
        """Move this entity to a specific coordinate on the current map.

        If the position is blocked, this entity will interact with the
        obstacle.
        """
        blocked_by = self.location.gamemap.is_blocked(x, y)
        if blocked_by and blocked_by is not True:
            target = blocked_by
            self.fighter.attack(target.fighter)
            self.end_turn()
        else:
            self.move_to(x, y)

    def attack_move_by(self, x:int, y:int) -> None:
        """Move this entity to a relative location, interacting with obstacles
        if necessary.
        """
        if x == 0 == y:
            self.end_turn()
            return
        self.attack_move_to(x + self.location.x, y + self.location.y)

    def move_to(self, x:int, y:int) -> None:
        """Move this entity to a specific coordinate on the current map.

        If the position is blocked, this entity will do nothing.
        """
        blocked_by = self.location.gamemap.is_blocked(x, y)
        if not blocked_by:
            self.location = self.location.gamemap.get_location(x, y)
            self.end_turn()

    def move_by(self, x:int, y:int) -> None:
        """Move this entity to a relative location."""
        self.move_to(x + self.location.x, y + self.location.y)

    def move_towards(self, x:int, y:int) -> None:
        """Move towards a specific location."""
        x -= self.location.x
        y -= self.location.y
        distance = math.hypot(x, y)
        self.move_by(round(x / distance), round(y / distance))

    def move_astar(self, x, y):
        """Move towards a position using AStar."""
        move_cost = self.location.gamemap.tiles['move_cost'].copy()

        for loc_xy, location in self.location.gamemap.entities.items():
            if location.get_blocking_entity():
                move_cost[loc_xy] += 10

        path = tcod.path.AStar(move_cost).get_path(*self.location.xy,
                                                   *g.player.location.xy)

        if path:
            self.move_to(*path[0])

    def distance_to(self, other) -> float:
        """Return the euclidean distance to another entity."""
        return math.hypot(other.location.x - self.location.x,
                          other.location.y - self.location.y)

    def end_turn(self):
        if self is g.player:
            g.state.on_enemy_turn()

    def die(self):
        if self is g.player:
            print('You died!')
            g.state = state.PlayerDeadState()
        else:
            print(f'{self.name} is dead!')
        self.ch = ord('%')
        self.fg = tcod.dark_red
        self.blocking = False
        self.render_order = 1
        self.fighter = None
        self.ai = None
        self.name = f'remains of {self.name}'
