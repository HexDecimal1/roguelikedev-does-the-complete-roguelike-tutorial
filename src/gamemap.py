
import random

import numpy as np
import tcod

import fighter
import g
import tile

class Location(object):

    def __init__(self, gamemap, x, y):
        self.gamemap = gamemap
        self.x = x
        self.y = y
        self.contents = []

    @property
    def xy(self):
        return self.x, self.y

    def get_blocking_entity(self):
        for entity in self.contents:
            if entity.blocking:
                return entity
        return None

class GameMap(object):

    def __init__(self, width, height):
        self.width = width
        self.height = height

        self.tiles = np.ndarray((width, height),
                                dtype=tile.TILE_DTYPE, order='F')

        self.tcod_map = tcod.map.Map(width, height, order='F')
        self.explored = np.zeros((width, height), dtype=np.bool_)

        self.entities = {}

    def make_map(self, max_rooms, room_min_size, room_max_size, player,
                 max_monsters_per_room):
        self.tiles[...] = tile.wall

        rooms = []

        for r in range(max_rooms):
            # random width and height
            w = random.randint(room_min_size, room_max_size)
            h = random.randint(room_min_size, room_max_size)
            # random position without going out of the boundaries of the map
            x = random.randint(0, self.width - w - 1)
            y = random.randint(0, self.height - h - 1)

            # "Rect" class makes rectangles easier to work with
            new_room = Rect(x, y, w, h)

            # Run through the other rooms and see if they intersect with this
            # one.
            if any(new_room.intersects(other_room) for other_room in rooms):
                continue
            # There are no intersections, so this room is valid.

            # "paint" it to the map's tiles
            self.tiles[new_room.slices] = tile.floor

            if len(rooms) == 0:
                # this is the first room, where the player starts at
                player.location = self.get_location(*new_room.center)
            else:
                # all rooms after the first:
                # connect it to the previous room with a tunnel
                corner = random.choice(
                    [
                    (rooms[-1].center[0], new_room.center[1]),
                    (new_room.center[0], rooms[-1].center[1]),
                    ]
                )

                self.create_tunnel(*rooms[-1].center, *corner)
                self.create_tunnel(*new_room.center, *corner)

            self.place_entities(new_room, max_monsters_per_room)

            # finally, append the new room to the list
            rooms.append(new_room)

    def create_tunnel(self, x1, y1, x2, y2):
        """Plot a tunnel using libtcod's line drawing function."""
        self.tiles[tcod.line_where(x1, y1, x2, y2)] = tile.floor

    def get_location(self, x, y):
        try:
            return self.entities[(x, y)]
        except KeyError:
            location = Location(self, x, y)
            self.entities[(x, y)] = location
            return location

    def is_blocked(self, x, y):
        if not (0 <= x < self.width and 0 <= y < self.height):
            return True
        blocking_entity = self.get_location(x, y).get_blocking_entity()
        if blocking_entity:
            return blocking_entity
        if self.tiles['move_cost'][x, y] == 0:
            return True
        return False

    def compute_fov(self):
        self.tcod_map.transparent[...] = self.tiles['transparent']
        self.tcod_map.compute_fov(
            *g.player.location.xy,
            radius=10,
            light_walls=True,
            algorithm=tcod.FOV_RESTRICTIVE,
        )
        self.explored |= self.tcod_map.fov

    def place_entities(self, room, max_monsters_per_room):
        # Get a random number of monsters
        number_of_monsters = random.randint(0, max_monsters_per_room)

        for i in range(number_of_monsters):
            # Choose a random location in the room
            location = self.get_location(
                random.randint(room.x1 + 1, room.x2 - 1),
                random.randint(room.y1 + 1, room.y2 - 1),
            )

            if location.contents:
                continue

            if random.random() < 0.8:
                fighter.Orc.spawn(location)
            else:
                fighter.Troll.spawn(location)

class Rect:
    def __init__(self, x, y, w, h):
        self.x1 = x
        self.y1 = y
        self.x2 = x + w
        self.y2 = y + h

    @property
    def center(self):
        center_x = int((self.x1 + self.x2) / 2)
        center_y = int((self.y1 + self.y2) / 2)
        return (center_x, center_y)

    @property
    def slices(self):
        """Returns slices used to index a Numpy array."""
        return slice(self.x1, self.x2), slice(self.y1, self.y2)

    def intersects(self, other):
        """ Returns true if this rectangle intersects with another one."""
        return (self.x1 <= other.x2 and self.x2 >= other.x1 and
                self.y1 <= other.y2 and self.y2 >= other.y1)
