
import g

class BasicMonster(object):

    def __init__(self):
        self.owner = None

    def take_turn(self):
        if not g.gamemap.tcod_map.fov[self.owner.location.xy]:
            return

        if self.owner.distance_to(g.player) < 2 and g.player.fighter.hp > 0:
            self.owner.fighter.attack(g.player.fighter)
            return

        self.owner.move_astar(*g.player.location.xy)
