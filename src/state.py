
import numpy as np
import tcod

import sdlevent
import g


def poll_events():
    for event in sdlevent.wait():
        if not g.state:
            return
        g.state.dispatch_event(event)


class EventDispatch(object):

    def __init__(self):
        pass

    def dispatch_event(self, event):
        getattr(self, f'on_{event.type.lower()}', self.on_other)(event)

    def on_keydown(self, event):
        pass

    def on_keyup(self, event):
        if (event.sym==sdlevent.K_RETURN and
                event.mod & (sdlevent.KMOD_LALT | sdlevent.KMOD_RALT)):
            tcod.console_set_fullscreen(not tcod.console_is_fullscreen())

    def on_textinput(self, event):
        pass

    def on_mousemotion(self, event):
        pass

    def on_other(self, event):
        print(event)

    def on_quit(self, event):
        raise SystemExit()

class State(EventDispatch):

    def render(self):
        pass

class GameState(State):
    MOVE = {
        sdlevent.K_PERIOD: (0, 0),

        sdlevent.K_UP: (0, -1),
        sdlevent.K_DOWN: (0, 1),
        sdlevent.K_LEFT: (-1, 0),
        sdlevent.K_RIGHT: (1, 0),
        sdlevent.K_HOME: (-1, -1),
        sdlevent.K_END: (1, -1),
        sdlevent.K_PAGEUP: (-1, 1),
        sdlevent.K_PAGEDOWN: (1, 1),

        sdlevent.K_KP_1: (-1, 1),
        sdlevent.K_KP_2: (0, 1),
        sdlevent.K_KP_3: (1, 1),
        sdlevent.K_KP_4: (-1, 0),
        sdlevent.K_KP_5: (0, 0),
        sdlevent.K_KP_6: (1, 0),
        sdlevent.K_KP_7: (-1, -1),
        sdlevent.K_KP_8: (0, -1),
        sdlevent.K_KP_9: (1, -1),

        sdlevent.K_h: (-1, 0),
        sdlevent.K_j: (0, 1),
        sdlevent.K_k: (0, -1),
        sdlevent.K_l: (1, 0),
        sdlevent.K_y: (-1, -1),
        sdlevent.K_u: (1, -1),
        sdlevent.K_b: (-1, 1),
        sdlevent.K_n: (1, 1),
    }

    def on_keydown(self, event):
        if event.sym == sdlevent.K_ESCAPE:
            raise SystemExit()
        elif event.sym in self.MOVE:
            g.player.attack_move_by(*self.MOVE[event.sym])
        else:
            super().on_keydown(event)

    def render(self):
        screen_tiles = g.gamemap.tiles
        view = slice(0, g.view_width), slice(0, g.view_height)
        g.console.bg[view] = np.where(g.gamemap.tcod_map.fov[..., np.newaxis],
                                      screen_tiles['bg'],
                                      screen_tiles['dark_bg'])
        g.console.fg[view] = screen_tiles['fg']
        g.console.ch[view] = screen_tiles['ch']
        g.console.fg[view][~g.gamemap.explored] = (0, 0, 0)
        g.console.bg[view][~g.gamemap.explored] = (0, 0, 0)

        for (x, y), loc in g.gamemap.entities.items():
            if not loc.contents:
                continue
            if not (0 <= x < g.screen_width and 0 <= y < g.screen_height):
                continue
            if not g.gamemap.explored[loc.xy]:
                continue
            entity = max(loc.contents, key=lambda obj:obj.render_order)
            g.console.fg[x, y] = entity.fg
            g.console.ch[x, y] = entity.ch

        tcod.console_flush()

    def on_enemy_turn(self):
        enemy_ai = []
        for location in g.gamemap.entities.values():
            for entity in location.contents:
                if entity is g.player:
                    continue
                if not entity.ai:
                    continue
                enemy_ai.append(entity.ai)
        for ai in enemy_ai:
            ai.take_turn()

class PlayerDeadState(GameState):

    def on_keydown(self, event):
        pass
