
import tcod

import fighter
import state
import g
import gamemap

def main():
    g.screen_width = 80
    g.screen_height = 50

    g.view_width = g.screen_width
    g.view_height = g.screen_height - 5

    g.player = fighter.Player.spawn(None)

    g.gamemap = gamemap.GameMap(g.view_width, g.view_height)
    g.gamemap.make_map(room_max_size=10, room_min_size=6,
                       max_rooms=30, player=g.player,
                       max_monsters_per_room=3)

    tcod.console_set_custom_font(
        'terminal8x12_gs_ro.png',
        tcod.FONT_LAYOUT_ASCII_INROW | tcod.FONT_TYPE_GREYSCALE
    )

    with tcod.console_init_root(g.screen_width, g.screen_height,
                                'libtcod tutorial revised',
                                False,
                                order='F') as g.console:
        g.state = state.GameState()
        while g.state:
            if g.fov_needs_regeneration:
                g.gamemap.compute_fov()
            g.state.render()

            state.poll_events()
