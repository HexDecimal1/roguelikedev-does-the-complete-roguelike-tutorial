
from dataclasses import dataclass, field
from typing import Any, Tuple

import tcod

import ai
import entity
import gamemap

@dataclass
class Fighter:
    name: str
    ch: int
    fg: Tuple[int, int, int]
    hp: int
    defense: int
    power: int
    AI: Any = ai.BasicMonster
    max_hp: int = None

    def __post_init__(self):
        if self.max_hp is None:
            self.max_hp = self.hp

    @classmethod
    def spawn(cls, location:gamemap.Location) -> entity.Entity:
        """Place a new fighter entity at `location`."""
        return entity.Entity(location, cls.ch, cls.fg, cls.name,
                             blocking=True, fighter=cls(), ai=cls.AI(),
                             render_order=3)

    def take_damage(self, amount):
        self.hp -= amount

        if self.hp <= 0:
            self.owner.die()

    def attack(self, target):
        damage = self.power - target.defense
        if damage > 0:
            target.take_damage(damage)
            print(
                f'{self.name} attacks {target.name} for {damage} hit points.')
        else:
            print(f'{self.name} attacks {target.name} but does no damage.')


@dataclass
class Player(Fighter):
    name: str = 'Player'
    ch: int = ord('@')
    fg: Tuple[int, int, int] = tuple(tcod.white)
    hp: int = 30
    defense: int = 2
    power: int = 5


@dataclass
class Orc(Fighter):
    name: str = 'Orc'
    ch: int = ord('o')
    fg: Tuple[int, int, int] = tuple(tcod.desaturated_green)
    hp: int = 10
    defense: int = 0
    power: int = 3


@dataclass
class Troll(Fighter):
    name: str = 'Troll'
    ch: int = ord('T')
    fg: Tuple[int, int, int] = tuple(tcod.darker_green)
    hp: int = 16
    defense: int = 1
    power: int = 4
